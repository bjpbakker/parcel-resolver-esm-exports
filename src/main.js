import { Resolver } from "@parcel/plugin";
import { stat } from "node:fs/promises";
import { dirname, join } from "node:path";

const resolvePackageNodeModulesDir = async (filename) => {
    let dir = dirname(filename);
    while (dir !== dirname(dir)) {
        try {
            const packageJson = await stat(join(dir, "package.json"));
            const nodeModules = await stat(join(dir, "node_modules"));
            if (packageJson.isFile() && nodeModules.isDirectory()) {
                return dir;
            }
        } catch (e) {
            if (e.code === 'ENOENT') {
                dir = dirname(dir);
                continue;
            }
            throw e;
        }
    }
    return null;
};

export default new Resolver({
    async resolve({dependency: {specifier, specifierType, resolveFrom}}) {
        if (specifierType !== "esm" && specifierType !== "commonjs") {
            return null;
        }

        // Resolve the dependency in the context of the package importing it to
        // follow linked packages.
        const fromPackage = await resolvePackageNodeModulesDir(resolveFrom);
        const paths = fromPackage != null ? [ join(fromPackage, "node_modules") ] : [];
        try {
            // Using require.resolve follows package.json exports
            const filePath = require.resolve(specifier, { paths });
            return { filePath };
        } catch (e) {
            if (e.code === 'MODULE_NOT_FOUND') {
                return null;
            }
            throw e;
        }
    }
});
