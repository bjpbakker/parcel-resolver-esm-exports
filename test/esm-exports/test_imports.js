import { patchBuiltins } from "@fpjs/overture/patches";

const main = () => {
    patchBuiltins ();
    console.log("Ok.");
}

main ();
