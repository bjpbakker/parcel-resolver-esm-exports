#!/usr/bin/env bash
# shellcheck shell=bash

set -eo pipefail

cprint() {
    c=$1
    s=$2
    if [ -t 1 ]; then
        printf "\033[%dm%s\033[0m\n" "$c" "$s"
    else
        printf "%s\n" "$s"
    fi
}

task() {
    name="$*"
    printf -- "  > %s.. " "$name"
    if out=$("$@" 2>&1); then
        cprint "32" "ok"
    else
        cprint "31" "failed"
        cat <<< "$out"
        return 1
    fi
}

main() {
    root=$(dirname "$0")
    mapfile -t tests < <(find "$root" -mindepth 1 -maxdepth 1 -type d)
    for test_dir in "${tests[@]}"; do
        test=$(basename "$test_dir")
        cprint "1" "$test"

        cd "$test_dir"
        task npm install
        task npm test
        cd - >/dev/null
        printf "\n"
    done
}

main "$@"
