[![pipeline status](https://gitlab.com/bjpbakker/parcel-resolver-esm-exports/badges/main/pipeline.svg)](https://gitlab.com/bjpbakker/parcel-resolver-esm-exports/-/commits/main)
[![npm package](https://img.shields.io/npm/v/parcel-resolver-esm-exports.svg)](https://www.npmjs.com/package/parcel-resolver-esm-exports)

# Parcel Resolver ESM Exports

Parcel Resolver plugin that resolves ESM exports.

## Usage

``` shell
> npm install --save-dev parcel-resolver-esm-exports
```

`.parcelrc`:
``` json
{
  "extends": "@parcel/config-default",
  "resolvers": [
    "...",
    "parcel-resolver-esm-exports"
  ]
}
```

## License

Copyright 2022-2023, Bart Bakker.

This software is subject to the terms of the Mozilla Public License, v. 2.0. If
a copy of the MPL was not distributed with this file, You can obtain one at
http://mozilla.org/MPL/2.0/.
